package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size for #id"() {
        when:
        def userId = new UserId(id)
        then:
        userId.correctSize == expectedResult
        where:
        id                  | expectedResult
        ''                  | false
        '12345'             | false
        '12345678912'       | true
        '9876543453421342'  | false
    }

    @Unroll
    def "should check sex for #id"() {
        when:
        def userId = new UserId(id)
        then:
        userId.sex.orElse(null) == expectedSex
        where:
        id                  | expectedSex
        ''                  | null
        '547'               | null
        '76030146492'       | UserIdChecker.Sex.MAN
        '83101878369'       | UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should check date for #id"() {
        when:
        def userId = new UserId(id)
        then:
        userId.date.orElse(null) == expectedDate
        where:
        id                  | expectedDate
        '92050505031'       | '05-05-1992'
        '84073170996'       | '31-07-1984'
        '95031203234'       | '12-03-1995'
        'qwertyzxcas'       | null
    }
}